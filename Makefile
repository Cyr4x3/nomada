# nomada - browser-indpendent terminal-based
# See LICENSE file for copyright and license details.

PROG = nomada

include config.mk

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp -f ${PROG} $(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)$(PREFIX)/bin/${PROG}
	cp -f dmenu-nomada $(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)$(PREFIX)/bin/dmenu-nomada
	sed "s/VERSION/$(VERSION)/g" < ${PROG}.1 > $(DESTDIR)$(MANPREFIX)/man1/${PROG}.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/${PROG}.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/${PROG}\
		$(DESTDIR)$(PREFIX)/bin/dmenu-nomada\
		$(DESTDIR)$(MANPREFIX)/man1/${PROG}.1

.PHONY: install uninstall
