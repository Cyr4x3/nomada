nomada
======

nomada ("nomad" in spanish) is a browser-independent and terminal-based
bookmark manager written in POSIX shell which aims to provide a simple and
minimal solution to bookmark management.


Build and install
-----------------

    $ git clone https://gitlab.com/Cyr4x3/nomada
    $ cd nomada
    # make install


Dependencies
------------

* POSIX compliant shell
* awk
* sed
* dmenu (optional. Required for dmenu-script.)
* xargs (optional. Required for dmenu-script.)

Although different implementations of awk and sed may work perfectly, only
[Gawk][0] and [GNU sed][1] (which are part of the GNU Core Utils) have been
tested.


Usage
-----

* nomada add -- add a new bookmark. The user will be prompted to enter the
  URL, the name and the protocol of the bookmark.

* nomada edit -- edit an already existing bookmark. The bookmark details will
  be opened in the text editor defined in the EDITOR environment variable
  (defaulting to vi in case EDITOR is not set).

* nomada export -- export a list of all currently saved bookmarks to a file.
  Currently supported file formats are gemtext, markdown and org. E.g.:

    nomada export org

  would print a list of the currently saved bookmarks formatted in
  org file syntax to stdout. The bookmark list can then be saved to a file
  with nomada export org > bookmarks.org.

* nomada rm -- remove a bookmark. When doing so, the user must specify its
  number (which can be seen via nomada list). E.g.: nomada rm 2 would
  remove bookmark 2.

* nomada list -- list all currently saved bookmarks.

* nomada name -- print the name of a bookmark. The bookmark number must be
  specified. E.g.: nomada name 5.

* nomada url -- print the url of a bookmark. The bookmark number must be
  specified. E.g.: nomada url 3.

More information can be obtained in the nomada(1) manual page.


Dmenu script
------------

Nomada also comes with a [dmenu][2] script, which makes it easier to interact
with your bookmarks. Once the user has added some bookmarks, dmenu-nomada can
be run. It will display a list of currently saved bookmarks in a similar way as
the nomada list command, which can then be selected for them to be opened in
the browser (just a bookmark at a time).

This script requires [dmenu][2] and [xargs][3] to be installed on the system.


Support
-------

If you need any help with nomada and/or wish to report a bug you can contact me
through [email][4].


Contributing
------------

Contributions and help in general are greatly appreciated. If you find any bug
or have any issue please report it through [email][4].
Any improvements or suggestions are also welcome.

Testing nomada in different Linux distributions and other *nix OSes would also
be great in order to make it as portable as possible.


License
-------

This project is licensed under the GNU GPL v3.0 License. See the LICENSE file.


[0]: https://www.gnu.org/software/gawk
[1]: https://www.gnu.org/software/sed
[2]: https://tools.suckless.org/dmenu
[3]: https://man.openbsd.org/xargs
[4]: mailto:cyr4x3@disroot.org
